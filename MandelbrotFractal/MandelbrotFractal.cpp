// MandelbrotFractal.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <fstream> // for files manipulation
#include <complex> // for complex numbers
#include <omp.h>

using namespace std;

float width = 600;
float height = 600;

int value(int x, int y) {
	complex<float> point((float)x / width - 1.5, (float)y / height - 0.5);
	complex<float> z(0, 0);
	unsigned int nb_iter = 0;
	while (abs(z) < 2 && nb_iter <= 34) {
		z = z * z + point;
		nb_iter++;
	}
	if (nb_iter < 34) return ((omp_get_thread_num()+1)*20 * nb_iter) / 33;
	else return 0;
}

int main() {	
	omp_set_num_threads(8); // *20 can't exceed 255
	
	ofstream my_Image("mandelbrot.ppm");
	if (my_Image.is_open()) {
		my_Image << "P3\n" << width << " " << height << " 255\n";
		int val;
		int lastSet = 0;
		int red = 0;
		int green = 0;
		int blue = 0;

		#pragma omp parallel for private(val) shared(lastSet)
		for (int i = 0; i < (int)width; i++) {
			for (int j = 0; j < (int)height; j++) {
				int test = omp_get_num_threads();
				val = value(i, j);									
				red = 255 * ((omp_get_thread_num() / omp_get_num_threads())) + val;
				//green = val * ((omp_get_thread_num() / omp_get_num_threads())) + red;
				blue = val * omp_get_thread_num();

				while (lastSet != (i*width)+j);				
				if (val > 0)
					my_Image << red << ' ' << green << ' ' << blue << "\n";
				else
					my_Image << 0 << ' ' << 0 << ' ' << 0 << "\n";

				lastSet += 1;
			}
		}
		my_Image.close();
	}
	else cout << "Could not open the file";
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
